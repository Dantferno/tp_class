import math

class Point:
    '''constructeur d’un point à partir de ses coordonnées cartésiennes'''
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.r = math.sqrt(x**2 + y**2)
        self.t = math.atan2(y,x)

    def x(self):
        '''renvoient les coordonnéescartésiennes du point'''
        return self.x

    def y(self):
        '''renvoient les coordonnéescartésiennes du point'''
        return self.y

    def __str__(self):
        '''renvoie une expression textuelle dupoint, comme ​"​(2.0, 3.0)​"'''
        return '({0}, {1})'.format(float(self.x), float(self.y))

    def __eq__(self, autrePoint):
        ''' ​a​ et ​b​ représentent-ils deuxpoints ​égaux​ ? '''
        try:
            return bool(self.x == autrePoint.x and self.y == autrePoint.y)
        except AttributeError:
            print('{0} n\'est pas comparable a {1}'.format(autrePoint, self))

    def homothetie(self, k):
        '''applique au point unehomothétie de centre (0, 0) et de rapport ​k'''
        self.x = k *self.x
        self.y = k *self.y


    def translation(self, dx, dy):
        '''applique au pointune translation de vecteur (​dx​, ​dy​)'''
        self.x = self.x + dx
        self.y = self.y + dy


class LignePol:
    '''représenterdetelsobjets.Les sommets y seront mémorisés sous forme de liste​ de points'''
    def __init__(self, *arg):
        for ar in arg:
            if not isinstance(ar, Point):
                raise Exception('Merci de fournir uniquement des points')
        self.sommets = [*arg]
        self.nb_sommets = len(self.sommets)
        print(*arg)

    def get_sommet(self, i):
        '''renvoie le i​ème​ sommet'''
        return str(self.sommets[i])

    def set_sommet(self, i, p):
        '''donne le point ​ppour valeur du i​ème​ sommet'''
        if isinstance(p, Point):
            self.sommets[i] = p
        else:
            Exception('Merci de fournir des points')

    def __str__(self):
        '''renvoie une expression de laligne polygonale sous forme de texte'''
        return 'Je suis une ligne composé des points : {}'.format([str(i) for i in self.sommets])

    def homothetie(self, k):
        '''applique à chaquesommet de la ligne polygonale une homothétie
        decentre (0, 0) et de rapport ​k'''
        for i in self.sommets:
            i.homothetie(k)

    def translation(self, dx, dy):
        '''applique àchaque sommet de la ligne polygonale unetranslation de vecteur (​dx​, ​dy​)'''
        for i in self.sommets:
            i.translation(dx, dy)


MALIGNE = LignePol(Point(2, 3), Point(1, 3), Point(2, 6))
MALIGNE.translation(2, 3)
print(str(MALIGNE))


class Pile:
    '''implementation d'une pile'''
    def __init__(self, liste):
        self.pile = liste

    def empiler(self, i):
        '''Ajoute a la fin'''
        self.pile.append(i)

    def depiler(self, i=1):
        '''supprime les i elements specifie a la fin de la pile'''
        try:
            self.pile = self.pile[:-i]
        except IndexError:
            raise Exception('Pas assez d\'element dans la file')

class File:
    '''implementation d'une file'''
    def __init__(self, liste):
        self.file = liste

    def enfiler(self, i):
        '''ajout au debut'''
        self.file.insert(0, i)

    def defiler(self):
        '''supprime dernier element'''
        self.file = self.file[:-1]

PILE = Pile([1, 23, 5, 116, 5])
